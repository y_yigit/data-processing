# -*- python -*-

from os.path import join

wdir = "/home/ubuntu/Desktop/Minor_DataMining/data-processing/Assignments/"
workdir: "/home/ubuntu/Desktop/Minor_DataMining/data-processing/Assignments/"
FASTQ_DIR = 'data/'
configfile: "config.yaml"

SAMPLES = config["samples"]

rule all:
    input:
        'output_assignment2/calls/all.vcf'

rule bwa_map:
    input:
        # Join example
        join(wdir, FASTQ_DIR, config["genome"]),
        # Workdir example
        FASTQ_DIR + "samples/{sample}.fastq"
    output:
        "output_assignment2/mapped_reads/{sample}.bam"
    message: "executing bwa mem on the following {input} to generate the following {output}"
    shell:
        "bwa mem {input} | samtools view -Sb - > {output}"

rule samtools_sort:
    input:
        "output_assignment2/mapped_reads/{sample}.bam"
    output:
        "output_assignment2/sorted_reads/{sample}.bam"
    message: "Sorting with Samtools on the following files {input}"
    shell:
        "samtools sort -T sorted_reads/{wildcards.sample} "
        "-O bam {input} > {output}"

rule samtools_index:
    input:
        "output_assignment2/sorted_reads/{sample}.bam"
    output:
        "output_assignment2/sorted_reads/{sample}.bam.bai"
    message: "Indexing with Samtools on the following files {input}"
    shell:
        "samtools index {input}"

rule bcftools_call:
    input:
        fa="data/genome.fa",
        bam=expand("output_assignment2/sorted_reads/{sample}.bam", sample=config["samples"]),
        bai=expand("output_assignment2/sorted_reads/{sample}.bam.bai", sample=config["samples"])
    output:
        "output_assignment2/calls/all.vcf"
    message: "Executing bcftools_call on the following files {input}"
    shell:
        "samtools mpileup -g -f {input.fa} {input.bam} | "
        "bcftools call -mv - > {output}"
        

rule report:
    input:
        "calls/all.vcf"
    output:
        "out.html"
    message: "Reporting outcome"
    run:
        from snakemake.utils import report
        with open(input[0]) as f:
            n_calls = sum(1 for line in f if not line.startswith("#"))

        report("""
        An example workflow
        ===================================

        Reads were mapped to the Yeas reference genome 
        and variants were called jointly with
        SAMtools/BCFtools.

        This resulted in {n_calls} variants (see Table T1_).
        """, output[0], metadata="Author: Mr Pipeline", T1=input[0])
      

